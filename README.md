This repository consists of simpler configuration for bash, emacs, web  and clojure suitable for virtual machines.

### Installation

Open a terminal and run the following command

    wget -qO- http://bit.ly/1jdhwxY > setup && bash setup

**Please Note** :

* No files will be deleted by the installer. All your current files will be backed up to ~/seartipy.backups directory.

* If ~/seartipy.backups already exists, it will be moved to trash. Also, if you find a file deleted but do not find it in ~/seartipy.backups, you should find it in  your trash folder.
