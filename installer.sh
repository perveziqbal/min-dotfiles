#!/bin/bash

log() {
    echo $1 | tee seartipy_installer.log
}

warn() {
    log "WARNING : $1"
}

err_exit() {
    log "FATAL: $1"
    exit 1
}

has_cmd() {
    command -v $1 > /dev/null
}

smd() {
    [ -d "$1" ] || mkdir -p "$1" 2> /dev/null
}

smv() {
    mv $1 $2 2> /dev/null
}

sln() {
    if ! [ -e "$1" ]; then
        warn "$1 does not exist, cannot create the link $2"
    else
        if [ -L "$2" ]; then
            trash-put "$2"
        elif [ -e "$2" ]; then
            warn "$2 exists and not a symbolic link! not creating link"
            return
        fi
    fi
    ln -s $1 $2
}

pre_cmd_check() {
    for cmd in $*; do
        has_cmd "$cmd" || err_exit "$cmd not be installed, quitting"
    done
}

pre_dir_check() {
    for dir in $*; do
        [ -d "$dir" ] || err_exit "$dir does not exist, quitting"
    done
}

cmd_check() {
    for cmd in $*; do
        has_cmd "$cmd" || warn "$cmd not installed"
    done
}

dir_check() {
    for dir in $*; do
        [ -d "$dir" ] || warn "$dir does not exist"
    done
}

sclone() {
    if ! [ -d "$2" ]; then
        log "Cloning $1 to $2"
        git clone $1 $2
    else
        log "skipping cloning $1 as $2 exists"
    fi
}

ln_check() {
    local link=`readlink -f $2`
    [[ "$1" == "$link" ]] ||  warn "$2 not a link to $1"
}

keep_sudo_running() {
    sudo -v
    while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2> /dev/null &
}

packages_install() {
    log "Essential packages installation"

    log "Updating ubuntu"
    if ! sudo apt-get update; then
        err_exit "apt-get update failed, quitting"
    fi

    log "Upgrading packages"
    if ! sudo apt-get upgrade -y; then
        err_exit "apt-get upgrade failed, quitting"
    fi

    log "Installing all packages, this will take a while"
    sudo apt-get install -y curl wget git trash-cli tree xsel xclip python-pip python-dev

    has_cmd git-up || sudo pip install git-up

    log "Cleaning apt"
    sudo apt-get autoremove -y
    sudo apt-get clean
    sudo apt-get autoclean
}

emacs_install() {
    log "Installing emacs packages"
    sudo apt-get install -y emacs24 silversearcher-ag

    [ -f ~/seartipy/min-dotfiles/emacs-init.el ] || return 1

    log "Moving ~/.emacs.d, ~/seartipy/emacses/min-emacs to $BACKUP_DIR (if they exist) "
    smv ~/.emacs.d $BACKUP_DIR
    smv ~/seartipy/emacses/min-emacs $BACKUP_DIR

    smd ~/seartipy/emacses/min-emacs

    log "Linking ~/seartipy/min-dotfiles/emacs-init.el as ~/seartipy/emacses/min-emacs/init.el "
    sln ~/seartipy/min-dotfiles/emacs-init.el ~/seartipy/emacses/min-emacs/init.el

    log "Linking ~/seartipy/emacses/min-emacs ~/.emacs.d "
    sln ~/seartipy/emacses/min-emacs ~/.emacs.d
}

clone_dotfiles() {
    log "Backing up your current min-dotfiles(if exists) to $BACKUP_DIR"
    smv ~/seartipy/min-dotfiles $BACKUP_DIR
    sclone https://pervezfunctor@github.com/pervezfunctor/min-dotfiles.git ~/seartipy/min-dotfiles

    pre_dir_check ~/seartipy/min-dotfiles
}

bash_install() {
    log "Installing bash"

    smv ~/seartipy/vendors/liquidprompt $BACKUP_DIR
    sclone https://github.com/nojhan/liquidprompt.git ~/seartipy/vendors/liquidprompt

    [ -f ~/seartipy/min-dotfiles/shellrc ] || return 1

    log "Moving ~/.bash_profile to $BACKUP_DIR"
    smv ~/.bash_profile $BACKUP_DIR
    sln ~/seartipy/min-dotfiles/shellrc ~/.bash_profile

    grep .bash_profile ~/.bashrc > /dev/null && return 1
    echo "[ -s ~/.bash_profile ] && source ~/.bash_profile" >> ~/.bashrc
}

script_options() {
    while [[ $# > 0 ]]; do
        case $1 in
            diagnostics)
                DIAGNOSTICS="diagnostics"
                shift
                ;;
            *)
                shift # ignore unknown option
                ;;
        esac
    done
}

setup_backup_dir() {
    BACKUP_DIR="$HOME/seartipy.backups"

    if [ -d "$BACKUP_DIR" ]; then
        log "moving $BACKUP_DIR to trash"
        trash-put $BACKUP_DIR
    fi

    smd $BACKUP_DIR
}

create_dirs() {
    setup_backup_dir

    smd $HOME/bin
    smd $HOME/seartipy/emacses
    smd $HOME/seartipy/vendors
}

installer() {
    script_options $*

    if [ -n "$DIAGNOSTICS" ]; then
        post_installer_check
        exit 0
    fi

    log "Installing..."
    keep_sudo_running

    create_dirs

    export PATH="$HOME/bin:$PATH"

    packages_install

    pre_installer_check

    clone_dotfiles

    emacs_install
    bash_install

    post_installer_check

    log "Installation done!!!"
    log "You could run 'bash ~/seartipy/min-dotfiles/installer.sh diagnostics' to check your installation"
}

pre_installer_check() {
    pre_cmd_check git curl wget trash-put
    pre_dir_check "$BACKUP_DIR" ~/bin ~/seartipy/emacses ~/seartipy/vendors
}

ubuntu_packages_check() {
    cmd_check curl wget git trash-put xsel xclip git-up
}

emacs_check() {
    cmd_check emacs ag
    ln_check ~/seartipy/emacses/min-emacs ~/.emacs.d
    ln_check ~/seartipy/min-dotfiles/emacs-init.el ~/seartipy/emacses/min-emacs/init.el
}

bash_check() {
    dir_check ~/seartipy/vendors/liquidprompt
    ln_check ~/seartipy/min-dotfiles/shellrc ~/.bash_profile
}

post_installer_check() {
    log "Running diagnostics"

    ubuntu_packages_check
    emacs_check
    bash_check

    log "diagnostics done!"
}

has_cmd trash-put && trash-put ~/seartipy-output.log  ~/seartipy-error.log 2> /dev/null

curdir=`pwd`

installer $* > >(tee ~/seartipy-output.log) 2> >(tee ~/seartipy-error.log >&2)

cd $curdir
