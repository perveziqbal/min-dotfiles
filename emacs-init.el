;; prefer UTF8

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(defconst seartipy-cache-directory
  (expand-file-name (concat user-emacs-directory ".cache/"))
  "storage area for persistent files")

(unless (file-exists-p seartipy-cache-directory)
  (make-directory seartipy-cache-directory t))

(setq custom-file (expand-file-name "custom.el" seartipy-cache-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; misc

(setq-default
 bookmark-default-file (expand-file-name ".bookmarks.el" seartipy-cache-directory)
 buffers-menu-max-size 30
 case-fold-search t
 column-number-mode t
 delete-selection-mode t
 indent-tabs-mode nil
 mouse-yank-at-point t
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil
 visible-bell t)

(setq transient-mark-mode t)

(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)

(setq indicate-empty-lines t)
(tool-bar-mode -1)
(set-scroll-bar-mode nil)
(menu-bar-mode -1)

(add-hook 'term-mode-hook
          (lambda ()
            (setq line-spacing 0)))

(electric-indent-mode t)
(setq create-lockfiles nil)
(fset 'yes-or-no-p 'y-or-n-p)

(windmove-default-keybindings)
(winner-mode)
(cua-selection-mode t)

;; Don't disable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)
;; Don't disable case-change functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(show-paren-mode 1)

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; slow (require 'tramp) fix
(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")

;; MELPA

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(use-package auto-package-update
  :ensure t
  :commands (auto-package-update-now))

;; UI

(use-package smart-mode-line
  :ensure t
  :config
  (smart-mode-line-enable))

(use-package solarized-theme :ensure t)
(load-theme 'solarized-light)

;; Essential

(use-package savehist
  :init
  (progn
    (setq savehist-file (concat seartipy-cache-directory "savehist")
          enable-recursive-minibuffers t ; Allow commands in minibuffers
          history-length 1000
          savehist-additional-variables '(mark-ring
                                          global-mark-ring
                                          search-ring
                                          regexp-search-ring
                                          extended-command-history)
          savehist-autosave-interval 60)
    (savehist-mode 1)))

(use-package saveplace
  :init
  (progn
    (setq save-place t
          save-place-file (concat seartipy-cache-directory "places"))))

(use-package recentf
  :config
  (setq recentf-save-file (concat seartipy-cache-directory "recentf")
        recentf-max-saved-items 100
        recentf-auto-save-timer (run-with-idle-timer 600 t 'recentf-save-list))

  (recentf-mode +1))

(use-package helm
  :ensure t
  :bind(("C-c h" . helm-command-prefix)
        ("M-x" . helm-M-x)
        ("M-y" . helm-show-kill-ring)
        ("C-x b" . helm-mini)
        ("C-x C-b" . helm-buffers-list)
        ("C-c f" . helm-recentf)
        ("C-x C-f" . helm-find-files))

  :init
  (setq helm-move-to-line-cycle-in-source     t
        helm-ff-search-library-in-sexp        t
        helm-ff-file-name-history-use-recentf t
        helm-prevent-escaping-from-minibuffer t
        helm-bookmark-show-location t
        helm-display-header-line nil
        helm-split-window-in-side-p t
        helm-always-two-windows t
        helm-echo-input-in-header-line t
        helm-imenu-execute-action-at-once-if-one nil

        ;; fuzzy matching setting
        helm-M-x-fuzzy-match t
        helm-apropos-fuzzy-match t
        helm-file-cache-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-lisp-fuzzy-completion t
        helm-recentf-fuzzy-match t
        helm-semantic-fuzzy-match t
        helm-buffers-fuzzy-matching t)

  (global-unset-key (kbd "C-x c"))

  :config
  (require 'helm-config)
  ;; Swap default TAB and C-z commands.
  ;; For GUI.
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
  ;; For terminal.
  (define-key helm-map (kbd "TAB") 'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-z") 'helm-select-action)
  (define-key minibuffer-local-map (kbd "C-c C-l") 'helm-minibuffer-history)
  (helm-autoresize-mode 1)
  (helm-mode 1))

(use-package helm-ag
  :ensure t
  :commands (helm-ag)
  :init
  (custom-set-variables
   '(helm-ag-use-aginore t)
   '(helm-ag-instert-at-point 'symbol)))

(use-package helm-swoop
  :ensure t
  :commands (helm-swoop helm-multi-swoop-all))

(use-package helm-descbinds
  :ensure t
  :bind (("C-h b" . helm-descbinds)))

;;;; Window management

(use-package uniquify
  :init
  (setq uniquify-buffer-name-style 'post-forward-angle-brackets
        uniquify-ignore-buffers-re "^\\*"))

;;;; Search

(use-package avy
  :ensure t
  :bind ("C-'" . avy-goto-word-or-subword-1)
  :init
  (progn
    (setq avy-keys (number-sequence ?a ?z))
    (setq avy-background t)))

;;;; Editing

(use-package which-key
  :ensure t
  :diminish which-key-mode " Ⓚ"
  :config
  (which-key-mode)

  (use-package whitespace-cleanup-mode
    :ensure t
    :defer t
    :diminish whitespace-cleanup-mode " ⓦ"
    :init
    (add-hook 'after-init-hook (lambda ()
                                 (global-whitespace-cleanup-mode)))
    (add-hook 'prog-mode-hook (lambda ()
                                (set-face-attribute 'trailing-whitespace nil
                                                    :background
                                                    (face-attribute 'font-lock-comment-face
                                                                    :foreground))
                                (setq show-trailing-whitespace 1)))))

(bind-key [remap just-one-space] 'cycle-spacing)
(bind-key "RET" 'newline-and-indent)

(use-package iedit
  :ensure t
  :diminish iedit-mode)

;; Git

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :init
  (setq-default
   magit-log-arguments '("--graph" "--show-signature")
   magit-process-popup-time 10
   magit-diff-refine-hunk t
   magit-push-always-verify nil)
  :config
  (global-git-commit-mode))

(use-package ediff
  :ensure t
  :init
  (progn
    (setq-default
     ediff-window-setup-function 'ediff-setup-windows-plain
     ediff-split-window-function 'split-window-horizontally
     ediff-merge-split-window-function 'split-window-horizontally)))

;;;; Programming

(use-package eldoc
  :diminish eldoc-mode
  :ensure t
  :init
  (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode))

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize)
  (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "JAVA_HOME" "JDK_HOME"))
    (add-to-list 'exec-path-from-shell-variables var)))

(use-package flycheck
  :ensure t
  :diminish flycheck-mode " ⓢ"
  :config
  (add-hook 'after-init-hook 'global-flycheck-mode)
  (setq flycheck-check-syntax-automatically '(save new-line mode-enabled)
        flycheck-idle-change-delay 0.8)
  (setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list))

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init
  (progn
    (setq-default projectile-enable-caching t)
    (setq projectile-sort-order 'recentf)
    (setq projectile-cache-file (concat seartipy-cache-directory
                                        "projectile.cache"))
    (setq projectile-known-projects-file (concat seartipy-cache-directory
                                                 "projectile-bookmarks.eld"))
    :config
    (projectile-global-mode)))

(use-package helm-projectile
  :ensure t
  :commands (helm-projectile)
  :config
  (helm-projectile-on))

(use-package company
  :diminish company-mode " ⓐ"
  :ensure t
  :commands (company-mode)
  :init
  (progn
    (setq company-idle-delay 0.5
          company-tooltip-limit 10
          company-minimum-prefix-length 2
          company-tooltip-flip-when-above t))
  :config
  (progn
    (setq company-minimum-prefix-length 2)
    (global-company-mode)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode " ⓟ"
  :commands (smartparens-mode smartparens-strict-mode)
  :init
  (progn
    (setq sp-show-pair-delay 0
          ;; no highlighting of region between parens
          sp-highlight-pair-overlay nil
          sp-highlight-wrap-overlay nil
          sp-highlight-wrap-tag-overlay nil

          sp-show-pair-from-inside nil
          sp-base-key-bindings 'paredit
          sp-autoskip-closing-pair 'always
          sp-hybrid-kill-entire-symbol nil
          sp-cancel-autoskip-on-backward-movement nil)
    (add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
    :config
    (progn
      (require 'smartparens-config)
      (sp-use-paredit-bindings)
      (show-smartparens-global-mode))))

(require 'init-local nil t)

;; ;;; init.el ends here
